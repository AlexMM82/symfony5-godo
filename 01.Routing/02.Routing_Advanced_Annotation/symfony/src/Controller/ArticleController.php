<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/{_locale}/article")
 */
class ArticleController extends AbstractController
{
    /**
     * Matches /en/article/2010/my-post
     * Matches /fr/article/2010/my-post.rss
     * Matches /en/article/2013/my-latest-post.html* 
     * @Route(
     *     "/{year}/{slug}.{_format}",
     *     name="article",
     *     defaults={"_format": "html"},
     *     requirements={
     *         "_locale": "en|fr",
     *         "_format": "html|rss",
     *         "year": "\d+"
     *     })
     */
    public function index($_locale, $year, $slug, $_format)
    {
        $message = 'Language Selected: '.$_locale.' Lamguage Selected: '.$year.'Slug Selected: '.$slug.' with format:'.$_format;
        return $this->json([
            'message' => $message,
            'path' => 'src/Controller/ArticleController.php',
        ]);
    }
}
