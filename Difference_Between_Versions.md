# Estructura de directorios 
 
## Directorios 
* *assets*
* *bin*
* *config*
* *public*
* *src*
* *templates*
* *tests*
* *translations*
* *var*
* *vendor*
 
La diferencia sustancial entre la estructura de un proyecto symfony3 y un proyecto symfony4 es que desaparece el bundle en nuestro proyecto. 

Seguimos teniendo la opción de incorporar bundles de terceros a nuestro proyecto, pero nuestro proyecto en sí ya no será un bundle. 

Esto ha permitido a los desarrolladores de Symfony poder sacar muchos elementos a directorios de primer nivel: 
* **templates**: *para las plantillas de Twig*
* **tests**: *para los tests*
* **translations**: *para los ficheros de diccionario*
* **config**: *para los ficheros de configuración*
* **assets**: *para los assets*
 
El directorio *web* se ha renombrado a *public*. 

## Archivos 
* *.env*
* *.env.dist*
* *.gitignore*
* *composer.json*
* *composer.lock*
* *package.json*
* *phpunit.xml.dist*
* *symfony.lock*
* *webpack.config.js*
