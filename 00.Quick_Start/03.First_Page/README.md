# Manual lanzar los proyectos symfony en Docker

1. Para una primera carga y generación de imágenes customizadas ejecutaremos `docker-compose -f docker-compose-mysql.yml -f docker-compose-symfony.yml build --no-cache`

2. Una vez tengamos las imágenes en nuestro registry simplemente usaremos `docker-compose -f docker-compose-mysql.yml -f docker-compose-symfony.yml up -d` para lanzar los contenedores.

3. Una vez levanatdos todos los contenedores accederemos dentro del contenedor de php, `docker exec -ti demo.symfony_php bash`, para instalar las dependencias de composer:

```bash
$ docker exec -ti demo.symfony_php bash
root@22bae2ec6ec6:/srv# composer install
```

> **NOTA**: Para instalar las dependencias usaremos `composer install`.