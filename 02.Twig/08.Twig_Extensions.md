# Propósito de la demostración - Twig Extensiones

En esta demo veremos como utilizar las extensiones dentro de **Twig**.

[Volver al Inicio](#-Propósito-de-la-demostración---Twig-Extensiones)

## ANTECEDENTES

---------------------------------------------------------------------------------------

Antes de crear una extensión de Twig, conviene comprobar si el filtro/función que necesitamos está ya implementado en las extensiones de Symfony o en las oficiales de Twig. 

Las extensiones oficiales de Twig se pueden encontrar en este enlace:

Fuente: [https://github.com/twigphp/Twig‐extensions](https://github.com/twigphp/Twig‐extensions)
 
y se instalan con el siguiente comando de composer:

```bash
composer require twig/extensions
```
 
### Creación de la clase 
 
Vamos a crear una extensión clásica que formatea un número en formato de moneda. Se utilizaría de la siguiente forma, con parámetros opcionales: 

```bash
{{ product.price|price }} 
{{ product.price|price(2, ',', '.') }}
```
 
Habría que empezar creando una clase que extienda de la **clase AbstractExtension** definida por **Twig** y programar la lógica.

```php
// src/Twig/AppExtension.php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    public function getFilters() 
    {
        return array( new TwigFilter('price', array($this, 'priceFilter')), );
    }
    public function priceFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = $price.'€'; 
        return $price; 
    }
}
```

La función `getFilters()` devuelve un array con la lista de filtros personalizados.

```php
    public function getFilters()
    {
        return array( 
            new TwigFilter('filtro1', array($this, 'filtro1Filter')),
            new TwigFilter('filtro2', array($this, 'filtro2Filter')),
            new TwigFilter('filtro3', array($this, 'filtro3Filter')),
            new TwigFilter('filtro4', array($this, 'filtro4Filter')),
        );
    }
```

Dicho array es un array de objetos TwigFilter, en los que se indica el nombre del filtro y el método de esta clase que implementa la lógica del filtro. 

Análogamente a `getFilters()`, se pueden implementar los métodos `getFunctions()`, `getGlobals()`, `getTokenParsers()`, `getOperators()` y `getTests()` para definir funciones, variables globales, tags, operadores y funciones de tests respectivamente.

Más documentación: [https://twig.symfony.com/doc/2.x/advanced.html#id1](https://twig.symfony.com/doc/2.x/advanced.html#id1) 

Una vez registrado el filtro y creado el método correspondiente, queda registrar la clase anterior como un servicio de Symfony.

> **NOTA**: Para que Symfony reconozca la clase como una extensión de Symfony, habría que registrarla en el fichero [services.yaml](services.yaml) con la etiqueta **twig.extension**. Sin embargo, si utilizamos la configuración por defecto de Symfony, este paso no es necesario. Symfony reconocerá automáticamente la clase como extensión de Twig. Entenderemos mejor esto cuando veamos el tema de Servicios.Con estos dos pasos, ya podemos utilizar el filtro price en cualquier plantilla. 

[Volver al Inicio](#-Propósito-de-la-demostración---Twig-Extensiones)

## RESUMEN DE LOS COMPONENTES DE SYMFONY PARA USAR

---------------------------------------------------------------------------------------

* Componente del servidor,`composer require server --dev`
* MakerBundle, `composer require symfony/maker-bundle --dev`, las herramientas **MakerBundle** necesitan de la instalación del componente **Annotations**.
* Annotations, `composer require annotations`.
* Twig Component, `composer require twig`
* Asset Component, `composer require symfony/asset`
* VarDumper, `composer require symfony/var-dumper`
* Profiler-pack, `composer require --dev symfony/profiler-pack`.
* Debug Bundle, `composer require symfony/debug-bundle`
* Twig Extensions, `composer require twig/extensions`

[Volver al Inicio](#-Propósito-de-la-demostración---Twig-Extensiones)

## INICIO DE LA DEMO

---------------------------------------------------------------------------------------

1. Crearemos nuestro proyecto usando el comando de la consola:

```bash
composer create-project symfony/skeleton 08.Twig_Extensions
```

2. En el siguiente paso, vamos a acceder a la carpeta del proyecto usando:

```bash
cd 08.Twig_Extensions
```

3. Luego, instalaremos el componente del servidor de Symfony usando el comando de la consola:

```bash
composer require server --dev
```

4. Ahora instalaremos el **MakerBundle**, `composer require symfony/maker-bundle --dev`, y **Annotations**, `composer require annotations`.

```bash
composer require symfony/maker-bundle --dev
composer require annotations
```

5. E instalaremos el componente de **Templates**, para ellos usaremos los comandos:

```bash
composer require twig
composer require symfony/asset
composer require twig/extensions
```

6. E instalaremos el componente de **VarDumper**, **Profiler-Pack** y **Debug-Bundle** con los comandos:

```bash
composer require symfony/var-dumper
composer require --dev symfony/profiler-pack
composer require symfony/debug-bundle
```

7. Ya podemos crear nuestro primer controlador, `php bin/console make:controller <controller-name>`. En esta demo utilizaremos `php bin/console make:controller DefaultController`

```bash
$ php bin/console make:controller DefaultController
 created: src/Controller/DefaultController.php
 created: templates/default/index.html.twig
  Success!
 Next: Open your new controller class and add some pages!
```

8. Ahora crearemos algunos filtros mediante el uso de las extensiones de **Twig**.

Nuestro primer filtro convertirá un precio a formato **$** o **€**, el cual será gestionado con el servicio [src/Twig/AppExtension.php](./src/Twig/AppExtension.php).

```php
<?php
// src/Twig/AppExtension.php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/* 
 * This extension of twig will only work with the following configuration in config/services.yaml.
 *  autoconfigure: true
 * If the autowiring in services.tml was not activated, the service would have to be declared.
 */ 
class AppExtension extends AbstractExtension
{ 
    public function getFilters() 
    { 
        return array( 
            new TwigFilter('priceEuro', array($this, 'priceEuroFilter')), 
            new TwigFilter('priceDolar', array($this, 'priceDolarFilter')), 
        ); 
    } 
    public function priceEuroFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',') 
    { 
        $price = number_format($number, $decimals, $decPoint, $thousandsSep); 
        $price = $price.'€'; 
        return $price; 
    } 
    public function priceDolarFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',') 
    { 
        $price = number_format($number, $decimals, $decPoint, $thousandsSep); 
        $price = '$'.$price; 
        return $price; 
    } 
} 
```

> **NOTA**: Como nuestra configuración relativa a los servicios en nuestro proyecto es **autowire: true**, no deberemos declarar la inyección de dependencias del servicio.

```yml
# Put parameters here that don't need to change on each machine where the app is deployed
# https://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
#...
services:
    # default configuration for services in *this* file
    _defaults:
        autowire: true      # Automatically injects dependencies in your services.
        autoconfigure: true # Automatically registers your services as commands, event subscribers, etc.
        public: false       # Allows optimizing the container by removing unused services; this also means
                            # fetching services directly from the container via $container->get() won't work.
                            # The best practice is to be explicit about your dependencies anyway.
#...
```

9. En el siguiente paso modificaremos nuestro controlador, [src/Controller/DefaultController.php](./src/Controller/DefaultController.php), para que envíe a la plantilla un valor que posteriormente filtraremos.


```diff
<?php
// src/Controller/DefaultController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/default", name="default")
     */
    public function index()
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
++          'price'=> 589.4145234523453,
        ]);
    }
}  
```

Y pasamos a utilizar la variable en Twig junto al filtro creado [src/Twig/AppExtension.php](./src/Twig/AppExtension.php):

```diff
{% extends 'base.html.twig' %}

{% block title %}Hello {{ controller_name }}!{% endblock %}

{% block body %}
<style>
    .example-wrapper { margin: 1em auto; max-width: 800px; width: 95%; font: 18px/1.5 sans-serif; }
    .example-wrapper code { background: #F5F5F5; padding: 2px 6px; }
</style>

<div class="example-wrapper">
--  <h1>Hello {{ controller_name }}! ✅</h1>
--  This friendly message is coming from:
--  <ul>
--      <li>Your controller at <code><a href="{{ 'src/Controller/DefaultController.php'|file_link(0) }}">src/Controller/DefaultController.php</a></code></li>
--      <li>Your template at <code><a href="{{ 'templates/default/index.html.twig'|file_link(0) }}">templates/default/index.html.twig</a></code></li>
--  </ul>
++    <p>{{price|priceDolar(2,'.',',')}}</p>
++    <p>{{price|priceEuro(2,',','.')}}</p>
</div>
{% endblock %}
```

10. Ahora ya podemos iniciar el servidor usando el comando:

```bash
php bin/console server:run
```

11. Finalmente, tendremos que hacer clic en el siguiente enlace [http://127.0.0.1:8000/default](http://127.0.0.1:8000/default) para ver el resultado.

[Volver al Inicio](#-Propósito-de-la-demostración---Twig-Extensiones)

## AMPLIANDO LA DEMO

---------------------------------------------------------------------------------------

Para afianzar conceptos crearemos otra extensión de Twig, que devuelva una fecha formateada.

Nuestro filtro será gestionado con el servicio [src/Twig/ElapsedTimeExtension.php](./src/Twig/ElapsedTimeExtension.php).

```php
<?php
// src/Twig/ElapsedTimeExtension.php
namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/* 
 * This extension of twig will only work with the following configuration in config/services.yaml.
 *  autoconfigure: true
 * If the autowiring in services.tml was not activated, the service would have to be declared.
 */ 
class ElapsedTimeExtension extends AbstractExtension
{
    
    /**
     * Declaración de los filtros que hay en este archivo
     */
    public function getFilters()
    {
        return array(
            new TwigFilter('tt', array($this, 'ttFilter')),
        );
    }  

    /**
     * Function that transforms the elapsed time into a user-friendly text
     *
     * @param DateTime $date
     * @return string
     */
    public function ttFilter(\DateTime $date): string
    {
        $interval = date_diff($date, new \DateTime());
        if ($interval->y >= 1) {
            return 'It`s '.$interval->y.' years';
        } elseif ($interval->m >= 1 && $interval->m < 12) {
            return 'It`s '.$interval->m.' months';
        } elseif ($interval->d >= 1 && $interval->d < 30) {
            return 'It`s '.$interval->d.' days';
        } elseif ($interval->h >= 1 && $interval->h < 24) {
            return 'It`s '.$interval->h.' hours';
        } elseif ($interval->i >= 1 && $interval->i < 60) {
            return 'It`s '.$interval->i.' minutes';
        } elseif ($interval->s < 60) {
            return 'Now';
        }
    }
}
```

A continuación modificaremos el controlador para que aporte ese valor a la plantilla.

```diff
<?php
// src/Controller/DefaultController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/default", name="default")
     */
    public function index()
    {
++      $date = \DateTime::createFromFormat('Y-m-d H:i','1985-11-08 09:01');
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'price'=> 589.4145234523453,
++          'date' => $date,
        ]);
    }
}  
```

E incluimos el dato en el template.

```diff
{% extends 'base.html.twig' %}

{% block title %}Hello {{ controller_name }}!{% endblock %}

{% block body %}
<style>
    .example-wrapper { margin: 1em auto; max-width: 800px; width: 95%; font: 18px/1.5 sans-serif; }
    .example-wrapper code { background: #F5F5F5; padding: 2px 6px; }
</style>

<div class="example-wrapper">
++  <p>Publish at {{ dump(date) }}</p>
++  <p>You born {{ date | tt }}</p>
    <p>{{price|priceDolar(2,'.',',')}}</p>
    <p>{{price|priceEuro(2,',','.')}}</p>
</div>
{% endblock %}

```

Ya podemos iniciar el servidor usando el comando:

```bash
php bin/console server:run
```

Finalmente, tendremos que hacer clic en el siguiente enlace [http://127.0.0.1:8000/default](http://127.0.0.1:8000/default) para ver el resultado.

[Volver al Inicio](#-Propósito-de-la-demostración---Twig-Extensiones)
