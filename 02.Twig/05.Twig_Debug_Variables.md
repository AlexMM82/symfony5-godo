# Propósito de la demostración - Twig Debug Variables

En esta demo veremos como **depurar variables** en **Twig**.

[Volver al Inicio](#-Propósito-de-la-demostración---Twig-Debug-Variables)

## ANTECEDENTES

---------------------------------------------------------------------------------------

En el componente **VarDumper** existe una función llamada `dump()` muy útil para la depuración de variables tanto en **twig** como en los controladores. Para utilizar esta función antes de nada tenemos que asegurarnos de tener el componente **VarDumper** isntalado:

```bash
composer require var‐dumper 
```

Ahora ya podemos utilizarlo. 

```php
// src/Controller/ArticleController.php 
namespace App\Controller; 
// … 
class ArticleController extends Controller
{ 
    public function recentList()
    { 
        $articles = …; 
        dump($articles); 
        // ... 
    } 
}
```

> **NOTA**: Para aprovechar esta funcionalidad al completo es necesario además disponer de los componentes de **Symfony**: **Profiler-pack** y **Debug Bundle**.

La función `dump()` renderiza el valor de la variable en la barra de depuración.

En twig tenemos dos formas de utilizar dump: 

* `{% dump foo.bar %}`
* `{{ dump(foo.bar) }}`

La primera de ellas, renderiza el valor de la variable en la barra de depuración, pero la segunda, renderiza el valor de la variable en el propio html.

La función `dump()` por defecto solamente está disponible en los entornos **dev** y **test**.

Utilizarla en el entorno de producción provocaría un error de PHP.

[Volver al Inicio](#-Propósito-de-la-demostración---Twig-Debug-Variables)

## RESUMEN DE LOS COMPONENTES DE SYMFONY PARA USAR

---------------------------------------------------------------------------------------

* Componente del servidor,`composer require server --dev`
* MakerBundle, `composer require symfony/maker-bundle --dev`, las herramientas **MakerBundle** necesitan de la instalación del componente **Annotations**.
* Annotations, `composer require annotations`.
* Twig Component, `composer require twig`
* Asset Component, `composer require symfony/asset`
* VarDumper, `composer require symfony/var-dumper`
* Profiler-pack, `composer require --dev symfony/profiler-pack`.
* Debug Bundle, `composer require symfony/debug-bundle`

[Volver al Inicio](#-Propósito-de-la-demostración---Twig-Debug-Variables)

## INICIO DE LA DEMO

---------------------------------------------------------------------------------------

1. Crearemos nuestro proyecto usando el comando de la consola:

```bash
composer create-project symfony/skeleton 05.Twig_Debug_Variables
```

2. En el siguiente paso, vamos a acceder a la carpeta del proyecto usando:

```bash
cd 05.Twig_Debug_Variables
```

3. Luego, instalaremos el componente del servidor de Symfony usando el comando de la consola:

```bash
composer require server --dev
```

4. Ahora instalaremos el **MakerBundle**, `composer require symfony/maker-bundle --dev`, y **Annotations**, `composer require annotations`.

```bash
composer require symfony/maker-bundle --dev
composer require annotations
```

5. E instalaremos el componente de **Templates**, para ellos usaremos los comandos:

```bash
composer require twig
composer require symfony/asset
```

6. E instalaremos el componente de **VarDumper**, **Profiler-Pack** y **Debug-Bundle** con los comandos:

```bash
composer require symfony/var-dumper
composer require --dev symfony/profiler-pack
composer require symfony/debug-bundle
```

7. Ya podemos crear nuestro primer controlador, `php bin/console make:controller <controller-name>`. En esta demo utilizaremos `php bin/console make:controller FirstController`

```bash
$ php bin/console make:controller FirstController
 created: src/Controller/FirstController.php
 created: templates/first/index.html.twig
  Success!
 Next: Open your new controller class and add some pages!
```

Y modificaremos el controlador [src/Controller/FirstController.php](src/Controller/FirstController.php) para probar esta funcionalidad del mismo:

```diff
<?php
// src/Controller/FirstController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FirstController extends AbstractController
{
    /**
     * @Route("/first", name="first")
     */
    public function index()
    {
++      $message = "new message";
++      dump($message);
        return $this->render('first/index.html.twig', [
            'controller_name' => 'FirstController',
        ]);
    }
}
```

Más su template [templates/first/index.html.twig](templates/first/index.html.twig), incluyendo **varDumper**:

```diff
{# templates/first/index.html.twig #}
{% extends 'base.html.twig' %}

{% block title %}Hello {{ controller_name }}!{% endblock %}

{% block body %}
<style>
    .example-wrapper { margin: 1em auto; max-width: 800px; width: 95%; font: 18px/1.5 sans-serif; }
    .example-wrapper code { background: #F5F5F5; padding: 2px 6px; }
</style>

<div class="example-wrapper">
--  <h1>Hello {{ controller_name }}! ✅</h1>
--  This friendly message is coming from:
--  <ul>
--      <li>Your controller at <code><a href="{{ 'src/Controller/FirstController.php'|file_link(0) }}">src/Controller/FirstController.php</a></code></li>
--      <li>Your template at <code><a href="{{ 'templates/first/index.html.twig'|file_link(0) }}">templates/first/index.html.twig</a></code></li>
--  </ul>
++  {# the contents of this variable are sent to the Web Debug Toolbar #}
++  {% dump controller_name %}
++  {# the contents of this variable are displayed on the web page #}
++  {{dump(controller_name)}}
</div>
{% endblock %}
```

8. Ahora ya podemos iniciar el servidor usando el comando:

```bash
php bin/console server:run
```

9. Finalmente, tendremos que hacer clic en el siguiente enlace [http://127.0.0.1:8000/first](http://127.0.0.1:8000/first) para ver el resultado.

[Volver al Inicio](#propósito-de-la-demostración---twig-ciclos-y-condicionales)